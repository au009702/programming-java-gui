package Drone_Simulation;
import java.util.Scanner;
//import java.util.*;

import javax.swing.JFileChooser;



//import javax.swing.JFileChooser;
import java.io.File;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.Scanner;


public class DroneInterface {
	JFileChooser chooser = new JFileChooser(); //For the file
	
	private Scanner s;								// Variable for scanner used for input from user
    private DroneArena myArena;				// Variable for the arena
 
    public DroneInterface() throws IOException{
    	 s = new Scanner(System.in);			// scanner set up
    	 myArena = new DroneArena(20, 6);	// creating the arena
    	
        char ch = ' ';
       
        do {
        	System.out.print("Enter (A)dd drone, get (I)nformation, (D)isplay, to (M)ove all the drones,  to create (N)ew arena, to (S)ave file, to load the (F)ile or e(X)it > "); //This is the menu
        	ch = s.next().charAt(0);
        	s.nextLine();
        	s.nextLine();
        	switch (ch) {
    			case 'A' :
    			case 'a' :
        					myArena.addDrone();	//'a' or 'A' to add a drone to arena
        					break;
        		case 'I' :
        		case 'i' :
        					System.out.print(myArena.toString()); //Printing the information of the arena and dornes
            				break;
        		case 'x' : 	ch = 'X';				// pressing 'x' or 'X' will end the program
        					break;
        		case 'd' :
        		case 'D' :
        			        doDisplay(); //Calls doDisplay to print the arena with the drones
        			        break;
        		case 'm' :
        		case 'M' :
        			        animate(); //Calls animate which will make the drones move
        			break;
        		case 'n' :
        		case 'N' :
        			newArena(); //Calls new arena to make the user create their arena
        			
        			break;
        			
        		case 'f' : //Pressing 'f' or 'F' to save the file
        		case 'F' :
        			int ret = chooser.showOpenDialog(null);
    				if (ret == JFileChooser.APPROVE_OPTION) {
    					File fileOne = chooser.getSelectedFile();					
    					if (fileOne.isFile()) {
    						try {
    							FileReader readFile = new FileReader(fileOne);
    							BufferedReader readBuff = new BufferedReader(readFile);
    							String string =  readBuff.readLine();
    			
    							Scanner arena = new Scanner(System.in);
    	        			   	System.out.println("Enter the x value of the new arena");
    	        			   	    int x = arena.nextInt();
    	        			    	System.out.println("Enter the y value of the new arena");
    	        			    	int y = arena.nextInt();
    	        			    	myArena = new DroneArena(Integer.valueOf(x), Integer.valueOf(y));    	        			    	    					    							   									
    								string = readBuff.readLine();
    								
    							
    							readBuff.close();
    						} catch (FileNotFoundException ex) {
    							ex.printStackTrace();
    						} catch (IOException ex) {
    							ex.printStackTrace();
    						}						
    					}
    					System.out.println("You chose to open: " + fileOne.getName());
    				}
    		
        			break;
        			
        		case 'S' :
        		case 's' :
        			int save = chooser.showSaveDialog(null);
    				if(save == JFileChooser.APPROVE_OPTION) {
    					File fileOne = chooser.getSelectedFile();
    					File dirFile = chooser.getCurrentDirectory();
    					File saving = new File(dirFile + "\\" + fileOne.getName());
    					
    					try {
    						FileWriter fileWriter = new FileWriter(saving);
    						PrintWriter writer = new PrintWriter(fileWriter);
    				
    						StringBuilder sss = new StringBuilder();
    				    	sss.append(myArena.getX() + " " + myArena.getY() + "\n");
    				    	for (Drone d: myArena.manyDrones) {
    				    		sss.append(d.getX() + " " + d.getY() + " " + d.getDirection() + "\n");
    				    	}
    						writer.print(sss.toString());
    						writer.close();
    					} catch(FileNotFoundException e) {
    						e.printStackTrace();
    					} catch(IOException e) {
    						e.printStackTrace();
    					}
    					System.out.println("You chose to save: " + fileOne.getName() + " in the dir " + dirFile.getAbsolutePath());
    				}
    			
    			
    

    				break;        
        	
    		} }while (ch != 'X');					

       s.close();		}							
    
    
        

    void doDisplay() { //Method to display the arena with the drones previously added to it
    	ConsoleCanvas c = new ConsoleCanvas(myArena.getX(), myArena.getY());
    	myArena.showDrones(c);
    	System.out.println(c.toString());
    }
    void animate() { //Method to make the drone to move around the arena
		for (int i = 0 ; i < 10; i++) {
			myArena.moveAllDrones(myArena);
			doDisplay();
			System.out.print(myArena.toString());
			
		}
	}
    
    void newArena() { //Method to ask the user to create their own arena
    	Scanner newArena = new Scanner(System.in);
    	System.out.println("Enter the x value of the new arena");
    	int x = newArena.nextInt();
    	System.out.println("Enter the y value of the new arena");
    	int y = newArena.nextInt();
    	myArena = new DroneArena(Integer.valueOf(x), Integer.valueOf(y));
    	
    }

    
	public static void main(String[] args) throws IOException{ //main method which calls the interface
		DroneInterface r = new DroneInterface();

	}

}


