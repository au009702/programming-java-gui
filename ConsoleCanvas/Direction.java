package Drone_Simulation;
import java.util.Random;


public enum Direction { //Enum class representing a group of constants
	North, East, South, West;
	
	public Direction next() {
		return values()[(ordinal() + 1) % values().length];
	}
	
	
	public Direction nextDir(Direction d) {
		switch (d) {
		case North:
			return East;
		case East:
			return South;
		case South:
			return West;
		case West:
			return North;
		}
		return d;
		}
	
	
	public Direction randomDir() { //Method to pick a random direction
		int pick = new Random().nextInt(Direction.values().length);
		return Direction.values()[pick];
	}
	
	public static void main(String[] args) { //Main method to print 'North', the next direction after 'North' which will be 'East', and a random direction
    	Direction d = Direction.North;
    	System.out.println(d.toString());
    	d = d.next();
    	System.out.println(d.toString());
        d = d.randomDir();
    	System.out.println(d.toString());


}}
