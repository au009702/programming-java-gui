package Drone_Simulation;

public class ConsoleCanvas {
	static char myArrays; //defining variables
static char[][] CanvasBig;
	
	ConsoleCanvas(int x1, int y1) { //This creates the arena for the console canvas
		CanvasBig = new char[x1][y1];
		for (int i = 0; i < x1; i++) {
			for (int j = 0; j < y1; j++) {
				if (j == 0 || j == y1 -1 || i == 0 || i == x1-1)CanvasBig[i][j] = '#';
				else CanvasBig[i][j] = ' ';
			}
		}
	}	

	public void showIt2(int k, int l, Direction dir_variable, char c) {	//This creates the first arena with '3 Drones' in it
		char[][] myArrays = new char[][] {{'#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#'},{'#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'},{'#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'},{'#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'},{'#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'},{'#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'},{'#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'},{'#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#'}};
		myArrays[4][3] = 'D';
		myArrays[4][5] = 'D';
		myArrays[5][2] = 'D';
		Drone d = new Drone(k, l, dir_variable);
		System.out.println("This is the big arena");
		for (char[] f:myArrays) {
			System.out.println(f);
		}		
	}
	
	public void showIt(int x1, int y1, Direction dir_variable, char c) {
		if (CanvasBig[x1][y1] == '#') return;
		Drone d = new Drone(x1, y1, dir_variable);
		Drone d2 = new Drone(x1, y1, dir_variable);
		Drone d3 = new Drone(x1, y1, dir_variable);
		
		CanvasBig[x1][y1] = c;
	}
		
	  public String toString() {  
			String g = new String("This is the second arena \n"); //This creates another secondary arena 
			for (int i = 0; i < CanvasBig[0].length ; i++) {
				for (int j = 0; j < CanvasBig.length; j++) {
					g+= CanvasBig[j][i];
				}
				g+= "\n";
			}
			
			return g;
		}
			
public static void main(String[] args) {
	ConsoleCanvas c = new ConsoleCanvas (10, 5);	// create a canvas
	c.showIt2(4, 3, Direction.West, 'D');								// add a Drone at 4,3
	System.out.println(c.toString());
					// display result
}
}



