package Drone_Simulation;

public class Drone { //Defining the variables
	private int x,y, droneTag; //creating its x,y position. Each drone will have a different droneTag.
	private int bx, by;
	private static int droneCount = 0; //the unique identifier
	Direction  dir_variable;
	
	public Drone (int dx, int dy, Direction dir) { //This is the constructor, droneCount will increment every time we call the constructor
		x = dx; //These are the coordinates for x and y
		y = dy;
		droneTag = droneCount++;
		bx = 1;
		by = 1;
		dir_variable = dir;
	}
	public Direction getDirection() { //Get function to get the direction
		return dir_variable;
	}

	public String toString() { //toString function that will return the position of each drone and its direction.
		return "Drone " + droneTag + " is at " + x + ", " + y + ", " + dir_variable.toString() + "\n";
	}
	
	public boolean isHere (int sx, int sy) { //boolean method to make sure we don't have two drones in the same position
		if (x == sx && y == sy) return true;
		else return false;
	}

	public void displayDrone(ConsoleCanvas c) { //These will display the drone in the console canvas class when we add a drone
		c.showIt(x,y, dir_variable, 'D');

	}
		
	public void tryToMove(DroneArena a) { // Method to make the drones move or to change their direction
		int plusx = x + bx;
		int minusx = x - bx;
		int plusy = y + by;
		int minusy = y - by;
		
		
		switch(dir_variable)
		{
			case North:
				if(a.canGoHere(x, y - by)) { y = minusy;}
				else {dir_variable = dir_variable.nextDir(dir_variable);}
				break;
			case East:
				if(a.canGoHere(x + bx, y)) {x = plusx; }
				else {dir_variable = dir_variable.nextDir(dir_variable);}
				break;
			case South:
				if(a.canGoHere(x, y + by)) { y = plusy;}
				else {dir_variable = dir_variable.nextDir(dir_variable);}
				break;
			case West:
				if(a.canGoHere(x-bx, y)) { x = minusx;}
				else {dir_variable = dir_variable.nextDir(dir_variable);}
				break;
		}
	}

public static void main(String[] args) {
	Drone d = new Drone(8, 5,Direction.North);		// create the drones in different positions and different Directions
	Drone d2 = new Drone(5, 4,Direction.West);
	Drone d3 = new Drone(2, 6,Direction.East);
	Drone d4 = new Drone(4, 9,Direction.South);
	Drone d5 = new Drone(10, 4,Direction.West);
	
	System.out.println(d.toString());	// Printing the different drones using the toString   
	System.out.println(d2.toString());
	System.out.println(d3.toString());
	System.out.println(d4.toString());
	System.out.println(d5.toString());
	
}

public int getX() {
	return x;
}
public int getY() {
	return y;
}




}
