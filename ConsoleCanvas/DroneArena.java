package Drone_Simulation;


import java.util.ArrayList;
import java.util.Random;

public class DroneArena { //defining the different variables
	private int dy, dx, nx, ny, bx, by; //variable defining the size of the Arena
	private Drone d; //this is just for a single drone
	private Random randomGenerator; //Declaring a random object
	public DroneArena showDrones;
	public static ArrayList<Drone> manyDrones;	
	Direction  dir_variable;
	public Drone b;
	DroneArena(int k, int l){
	nx = k;
	ny = l;
	bx = k -1;
	by = l -1;
	manyDrones = new ArrayList<Drone>(); //creating an arraylist
 }

	public Drone getDroneAt(int x, int y) { //Making sure we don't have two drones on the same position
		for (Drone dr: manyDrones) {
			if (dr.getX() == x && dr.getY() == y)return dr;
		}
		return null;
	}	
	public int getX() { //getFunction
		return nx;
		
	}
	public int getY() { //getFunction
		return ny;
		
	}
	public void moveAllDrones(DroneArena arena) {
		for (Drone m : arena.manyDrones)m.tryToMove(arena); 
	}
	public boolean canGoHere(int x, int y) { //Seeing if a drone can go to that position
		for (Drone m : manyDrones) 
		{if (m.isHere(x,  y) == true 
		|| x<0 || x==0  
		|| y<0 || y==0
		|| x>bx || x==bx
		|| y>by || y==by) 
		
		return false;
		}
		return true;
		}
	

public void addDrone () { //This is the method for the drone
	randomGenerator = new Random(); //This is to create a random object
	dx = randomGenerator.nextInt(bx);
	dy = randomGenerator.nextInt(by);
	do { dx = randomGenerator.nextInt(bx);//These two lines of code are to get a random integer
	dy = randomGenerator.nextInt(by);
	}
	while (getDroneAt(dx,dy) != null);
	//Position so it is in the middle of the arena
	d = new Drone(dx, dy, Direction.South);
	manyDrones.add(d);
}
	public void addDrone(int k, int l, Direction c) { //Adding a drone into the array
		Drone d = new Drone(k, l, c);
		manyDrones.add(d);
	}

public String toString() { //To string that prints the arena and the position of the drone
	String o = "The Arena Size is " + nx + " by " + ny + "\n";
	for (Drone x : manyDrones) o += x.toString(); //looks at the drone class
	return o;
	
}

public void showDrones(ConsoleCanvas c) { //Showing drones in the console canvas
	for (Drone m : manyDrones) m.displayDrone(c);
}

public static void main(String[] args) {
	DroneArena d = new DroneArena(10, 5);	// create drone arena and adding drones to it
	{
		d.addDrone();
		d.addDrone();
		d.addDrone();
		d.addDrone();
		d.addDrone();
		d.addDrone();
		
		System.out.println(d.toString()); //Printing the toString
		
		for (int ct = 0; ct<10; ct++) { //Moving the drones around
			d.moveAllDrones(d);
			System.out.println(d.toString());			
		}		
	}}}
	
