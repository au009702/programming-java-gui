package DronesGUIProject;

public abstract class Drone { // Defining variables
	private static int DroneCount = 0;	//Unique identifier	for the drones and obstacles				
	protected int DroneTag;
	protected double x, y, rd; //Creating the x,y and radious positions for the drone					
	protected char colorr;	//variable for the color of the Drone														
	Drone() {}
	/**
	 * create main drone with variables
	 * @param dx
	 * @param dy
	 * @param dr
	 */
	Drone (double dx, double dy, double dr) { //Constructor with droneCount incrementing everytime we call a drone
		x = dx;
		y = dy;
		DroneTag = DroneCount++; //This will increment the Count for every Drone
		rd = dr;		
		colorr = 'r'; 
	}
	
	protected void drawDrone(MyDroneCanvas mc) { //This will draw every drone and obstacle with their respecting positions and colour
		mc.showDrone(x, y, rd, colorr);
	}
	protected String getStrType() { //get Function as a string that will display in the GUI
		return "Drone";
	}
	
	public String toString() { //toString that will display in the GUI with the name of the object and their position on the Arena
		return getStrType()+" at "+Math.round(x)+", "+Math.round(y);
	}
	
	protected abstract void checkDrone(DroneArena b); //This method is abstract and will check if the drone is in the arena
	protected abstract void adjustDrone();

	
	protected boolean hittingDrone(double ox, double oy, double or) { //Hitting drone method checks if the drone is hitting with another obstacle / drone
		return (ox-x)*(ox-x) + (oy-y)*(oy-y) < (or+rd)*(or+rd);
	}		
	
	
	public boolean hittingDrone (Drone oDrone) {
		return hittingDrone(oDrone.getX(), oDrone.getY(), oDrone.getRad());
	}


                                     //Getter functions and set functions
protected double getX() { return x; }	//This will return the 'x' position of the drone
protected double getY() { return y; } //This will return the 'y' position of the drone
protected double getRad() { return rd; } //This will return the radious of the drone as it is an sphere
protected int getID() {return DroneTag; } //This will return the DroneTag of the drone which is its identification


protected void setXY(double dx, double dy) { //This is a set function for the drone
	x = dx;
	y = dy;
}
}
