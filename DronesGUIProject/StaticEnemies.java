package DronesGUIProject;


import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class StaticEnemies extends Drone { //This class extends from the drone and relates to the static enemies
		

	public StaticEnemies(double dx, double dy, double dr) {
		super(dx, dy, dr);
		colorr = 'c';	
	}

	protected void checkDrone(DroneArena d) { 	 
		if (d.DroneHit(this)) EndP(null);   			
	}
		
	private void EndP(Stage stage){
		Alert endAlert = new Alert(AlertType.INFORMATION, "Game Over");
		endAlert.setTitle("Game Over");
		endAlert.setHeaderText(null);
	
		endAlert.setOnCloseRequest(null);
		endAlert.show();
		System.exit(0);
				
	}
	protected void adjustDrone() {
		 
	}

	protected String getStrType() {
		return "Static Enemy";
	}	


	
}