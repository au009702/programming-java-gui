package DronesGUIProject;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class RandomEnemies extends Drone { //This class extends from the drone and relates to the creation of random enemies
	

	public RandomEnemies(double bx, double by, double br) {
		super(bx, by, br);
		colorr = 'c'; 
	}
	
	protected void checkDrone(DroneArena b) {
        if (b.DroneHit(this)) EndP(null);		
	}

	private void EndP(Stage stage){
		Alert endAlert = new Alert(AlertType.INFORMATION, "Game Over");
		endAlert.setTitle("Game Over");
		endAlert.setHeaderText(null);
		endAlert.setOnCloseRequest(null);
		endAlert.show();
		System.exit(0);
		


		
	}
	
	protected void adjustDrone() {	
	}

	protected String getStrType() {
		return "Random Enemies";
	}	

}
