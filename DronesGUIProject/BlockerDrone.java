package DronesGUIProject;

public class BlockerDrone extends Drone {
	
	//This class extends from the Drone class and gets in the way of the drone

	protected BlockerDrone(double bx, double by, double br) {
		super(bx, by, br);
		colorr = 'd';
	}
	protected void adjustDrone() {}

	protected void checkDrone(DroneArena b) {}

	protected String getStrType() {
		return "Blocker";
	}	
}