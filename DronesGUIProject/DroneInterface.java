package DronesGUIProject;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;


import javax.swing.JFileChooser;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
//**

 


public class DroneInterface extends Application { //Setting the variables for timer arena etc
	private MyDroneCanvas dc;
	private AnimationTimer Dronetimer;								
	private VBox DronePane;										
	private DroneArena arena;
	JFileChooser chooser = new JFileChooser();

	
	private void AboutDrone() { //Function to create the About alert
	    Alert alert = new Alert(AlertType.INFORMATION);				
	    alert.setTitle("About");									
	    alert.setHeaderText(null);			
		alert.setContentText("29009702 JavaFX Demonstrator");			
	    alert.showAndWait();										
	}

	private void LoadFile(){ //LoadFile function
		
		int ret = chooser.showOpenDialog(null);
    				if (ret == JFileChooser.APPROVE_OPTION) {
    					File fileOne = chooser.getSelectedFile();					
    					if (fileOne.isFile()) {
    						try {
    							FileReader readFile = new FileReader(fileOne);
    							BufferedReader readBuff = new BufferedReader(readFile);


    							//String string =  readBuff.readLine();
    							//Scanner arena = new Scanner(System.in);
    	        			  // 	System.out.println("Enter the x value of the new arena");
    	        			   	   // int x = arena.nextInt();
    	        			    //	System.out.println("Enter the y value of the new arena");
    	        			    //	int y = arena.nextInt();    	        			    	    					    							   									
    								//string = readBuff.readLine();
    								
    							
    							readBuff.close();
    						} catch (FileNotFoundException ex) {
    							ex.printStackTrace();
    						} catch (IOException ex) {
    							ex.printStackTrace();
    						}						
    					}
    					System.out.println("You chose to open: " + fileOne.getName());
    				}
	}

	private void SaveFile(){ //Save file function

		int save = chooser.showSaveDialog(null);
    				if(save == JFileChooser.APPROVE_OPTION) {
    					File fileOne = chooser.getSelectedFile();
    					File dirFile = chooser.getCurrentDirectory();
    					File saving = new File(dirFile + "\\" + fileOne.getName());
    					
    					try {
    						FileWriter fileWriter = new FileWriter(saving);
    						PrintWriter writer = new PrintWriter(fileWriter);
    				
    						StringBuilder sss = new StringBuilder();
    				    	sss.append(DroneArena.getX() + " " + DroneArena.getY() + "\n");
    				    	for (Drone d: DroneArena.allDrones) {
    				    		sss.append(d.getX() + " " + d.getY() + " " + "\n");
    				    	}
    						writer.print(sss.toString());
    						writer.close();
    					} catch(FileNotFoundException e) {
    						e.printStackTrace();
    					} catch(IOException e) {
    						e.printStackTrace();
    					}
    					System.out.println("You chose to save: " + fileOne.getName() + " in the dir " + dirFile.getAbsolutePath());
    				}
	}


	private void RulesDrone() { //Function to create the rules alert
	    Alert alert = new Alert(AlertType.INFORMATION);				
	    alert.setTitle("Rules");									
	    alert.setHeaderText(null);			
		alert.setContentText("The objective of the game is to make the drone grab the Gold coin avoiding the red enemies.                                                    There are 3 different difficulties in which you can prove yourself:                                                                                        1. EASY: No obstacles and enemies in random positions or moving.                                                                                         2. MEDIUM: Adding moving enemies and obstacles                                                                                        3. HARD: Moving enemies and obstacles + adding enemies and obstacles in random positions");			
	    alert.showAndWait();										
	}
    /**
	  * set up the paddle, when the user clicks on screen, paddle will go there
	  * @param canvas
	  */
	void setMouseEvents (Canvas canvas) { //Function from when the mouse is pressed (get the X and Y positions of the setCursor function from the DroneArena Class)
	       canvas.addEventHandler(MouseEvent.MOUSE_PRESSED, 		
	    	       new EventHandler<MouseEvent>() {
	    	           @Override
	    	           public void handle(MouseEvent e) {
	    	        	   	arena.setCursor(e.getX(), e.getY());
	  		            	drawDroneWorld();							
	  		            	drawStatus();
	    	           }
	    	       });
	}

	MenuBar setMenu() { //To create the menu with the two buttons 'File' with its item 'Exit' and 'Help' with items 'About' and 'Rules' 
		MenuBar menuBar = new MenuBar();						
	
		Menu mFile = new Menu("File");							
		MenuItem mExit = new MenuItem("Exit");
		MenuItem mLoad = new MenuItem("Load");
		MenuItem mSave = new MenuItem("Save");					
		mExit.setOnAction(new EventHandler<ActionEvent>() { //Add the exit Button
		    public void handle(ActionEvent t) {					
	        	Dronetimer.stop();									
		        System.exit(0);									
		    }
		});

		mLoad.setOnAction(new EventHandler<ActionEvent>() { // Add the Load button
		    public void handle(ActionEvent t) {	
				LoadFile();				
	        									
		    }
		});


		mSave.setOnAction(new EventHandler<ActionEvent>() { //Add the save button
		    public void handle(ActionEvent t) {	
				SaveFile();				
	        									
		    }
		});

		mFile.getItems().addAll(mExit, mLoad, mSave); //Add the subItems to the file button							
		
		Menu mHelp = new Menu("Help");							
		MenuItem mAbout = new MenuItem("About");
		MenuItem mRules = new MenuItem("Rules");				
		mAbout.setOnAction(new EventHandler<ActionEvent>() {
            
            public void handle(ActionEvent actionEvent) {
            	AboutDrone();
													
            }
		
		});

		mRules.setOnAction(new EventHandler<ActionEvent>() { //Add the rules button
            
            public void handle(ActionEvent actionEvent) {
            	RulesDrone();
													
            }
		
		});

			
		mHelp.getItems().addAll(mAbout, mRules);
		menuBar.getMenus().addAll(mFile, mHelp);				
		return menuBar;											
	}

	
	private HBox setButtons() {
	    Button btnStart = new Button("Start");	//Create button "Start" that will start the drone and moving enemies				
	    btnStart.setOnAction(new EventHandler<ActionEvent>() {	
	        
	        public void handle(ActionEvent event) {
	        	Dronetimer.start();									
	       }
	    });

	    Button btnStop = new Button("Pause Game"); //Create button "Pause" that will pause the drone and moving enemies					
	    btnStop.setOnAction(new EventHandler<ActionEvent>() {
	        
	        public void handle(ActionEvent event) {
	           	Dronetimer.stop();									
	       }
	    });

	    Button btnAdd = new Button("Add moving enemies and Obstacles");	//Creates the button "Add moving enemies" which will call addDrone in DroneArena			
	    btnAdd.setOnAction(new EventHandler<ActionEvent>() {
	        
	        public void handle(ActionEvent event) {
	           	arena.addDrone();								
	           	drawDroneWorld();
	       }
	    });

		Button btnAdd2 = new Button("Add an Obstacle");	//Create the button "Add an Obstacle" which will call addObstacle in DroneArena that creates obstacles in random positiosn			
	    btnAdd2.setOnAction(new EventHandler<ActionEvent>() {
	        
	        public void handle(ActionEvent event) {
	           	arena.addObstacle();								
	           	drawDroneWorld();
	       }
	    });

		Button btnAdd3 = new Button("Add a random enemy");	//Create the button "Add an Obstacle" which will call addObstacle in DroneArena that creates obstacles in random positiosn			
	    btnAdd3.setOnAction(new EventHandler<ActionEvent>() {
	        
	        public void handle(ActionEvent event) {
	           	arena.addREnemies();								
	           	drawDroneWorld();
	       }
	    });
   														
	    return new HBox(new Label("Run: "), btnStart, btnStop, new Label("Add: "), btnAdd, btnAdd2, btnAdd3 //Adding the labels and buttons on the box 
		);
	}

	protected void drawDroneWorld () { //creating the world
	 	dc.clearDroneCanvas();						
	 	arena.drawArena(dc);
		
	}
	
	protected void drawStatus() {
		DronePane.getChildren().clear();					
		ArrayList<String> allDs = arena.describeAll();
		
		for (String s : allDs) {
			Label l = new Label(s); 		
			DronePane.getChildren().add(l);		
		}	
		
	}
	
	public void start(Stage primaryStage) throws Exception { //Main stage which is primaryStage
		
		primaryStage.setTitle("29009702 attempt at Paddle pong alternative game"); // To set up the title of the primary stage
	    BorderPane da = new BorderPane();
	    da.setPadding(new Insets(10, 20, 10, 20));

	    da.setTop(setMenu());
			 

		Image dronepic = new Image("C:/Users/joanc/OneDrive/Desktop/dronepic.jpg"); //For the icon image
		primaryStage.getIcons().add(dronepic);

	    Group root = new Group();			//To create a Group							
	    Canvas canvas = new Canvas(500, 600); //To create a Canvas
	    root.getChildren().add( canvas );
	    da.setLeft(root);												
	
	    dc = new MyDroneCanvas(canvas.getGraphicsContext2D(), 500, 600);
		

	    setMouseEvents(canvas);											

	    arena = new DroneArena(500, 600);	//To set up the arena							
	    drawDroneWorld();
	    
	    Dronetimer = new AnimationTimer() {		 //To set up the timer							
	        public void handle(long currentNanoTime) {					
	        		arena.checkDrone();									
		            arena.adjustDrone();								
		            drawDroneWorld();										
		            drawStatus();
														
	        }
	    };

	
	    DronePane = new VBox();											
		DronePane.setAlignment(Pos.TOP_LEFT);								
		DronePane.setPadding(new Insets(5, 75, 75, 5));					
 		da.setRight(DronePane);											
		  
	    da.setBottom(setButtons());
		
		StackPane holder = new StackPane(); //Create a new StackPane setting the colour to 'Silver'
		holder.getChildren().add(canvas);
        root.getChildren().add(holder);
        Image img = new Image("C:/Users/joanc/Downloads/clipart12876.png");
        ImageView imgView = new ImageView(img);
		imgView.setFitHeight(100);
        imgView.setFitWidth(500);
		imgView.setX(200);
		imgView.setY(200);
        holder.getChildren().add(imgView);
		holder.setStyle("-fx-background-color: SILVER");

	    Scene scene = new Scene(da, 700, 700);				//This will set the scene			
        da.prefHeightProperty().bind(scene.heightProperty());
        da.prefWidthProperty().bind(scene.widthProperty());

        primaryStage.setScene(scene);
        primaryStage.show();
	  
	}
	
	public static void main(String[] args) { //Main method to lounch the application
	    Application.launch(args);			

	}

}