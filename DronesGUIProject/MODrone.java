package DronesGUIProject;



public class MODrone extends Drone { //This class extends from the Drone and relates to the moving obstacles

	double dAngle, dSpeed;			
	
		public MODrone(double dx, double dy, double dr, double da, double ds) {
		super(dx, dy, dr);
		dAngle = da;
		dSpeed = ds;
        colorr = 'g';	
	}	
    protected void checkDrone(DroneArena d) {
        if (d.DroneHit(this));			
        dAngle = d.CheckDroneAngle(x, y, rd, dAngle, DroneTag);}
	
	protected void adjustDrone() {
        double radAngle = dAngle*Math.PI/180;		
		x += dSpeed * Math.cos(radAngle);		
		y += dSpeed * Math.sin(radAngle);
				
	}
	protected String getStrType() {
		return "Moving obstacle";
	}
}