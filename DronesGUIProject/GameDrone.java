package DronesGUIProject;


public class GameDrone extends Drone { //This class extends from the Drone class and relates to the Main drone

	double dAngle, dSpeed;			
	

	public GameDrone(double dx, double dy, double dr, double da, double ds) {
		super(dx, dy, dr);
		dAngle = da;
		dSpeed = ds;
		colorr = 'i';
	}

	protected void checkDrone(DroneArena d) {
		dAngle = d.CheckDroneAngle(x, y, rd, dAngle, DroneTag);
	}
		
	protected void adjustDrone() {
		double radAngle = dAngle*Math.PI/180;		
		x += dSpeed * Math.cos(radAngle);		
		y += dSpeed * Math.sin(radAngle);		
	}
		
	protected String getStrType() {
		return "Game Drone";
	}

}
