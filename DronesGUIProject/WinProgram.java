package DronesGUIProject; //These class extends from the drone and relates to the win objective

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class WinProgram extends Drone {

	
	protected String getStrType() {
		return "Win Objective";
	}

	public WinProgram(double dx, double dy, double dr) {
		super(dx, dy, dr);
		colorr = 'h';	
	}
	
	protected void checkDrone(DroneArena b) {
		if (b.DroneHit(this)) WinP(null);			
	}

	private void WinP(Stage stage){
		Alert endAlert = new Alert(AlertType.INFORMATION, "Congratulations you finished the game!!");
		endAlert.setTitle("Congratulations!");
		endAlert.setHeaderText(null);
		
		endAlert.setOnHidden(evt -> System.exit(0));
		endAlert.setOnCloseRequest(null);
		endAlert.show();
		


		
	}
  



		
	protected void adjustDrone() {	 
	}

	
}