package DronesGUIProject;

import java.util.ArrayList;
/**
	 * Create the arena with size xD and yD
	 * @param xD
	 * @param yD
	 */
public class DroneArena {	
	static double xArenaSize; //variable that show the size of the arena			
	static double yArenaSize;
	static ArrayList<Drone> allDrones;	 //variable for the Arraylist of the different drones/obstacles in the arena
	
	DroneArena() {				
	}

	DroneArena(double xD, double yD){ //To set how big the arena is, and to add all the different types of drones and obstacles with their different positions
		xArenaSize = xD;
		yArenaSize = yD;
		allDrones = new ArrayList<Drone>();					
		allDrones.add(new GameDrone(xD/1.99, yD/1.05, 10, 45, 3));
		allDrones.add(new WinProgram(xD/2, yD/4, 5));
		allDrones.add(new CursorDrone(xD/2, yD-5, 13));
		
		allDrones.add(new BlockerDrone(xD/2.2, yD/1.1, 12));
		allDrones.add(new BlockerDrone(xD/2.2, yD/1.005, 12));
		allDrones.add(new BlockerDrone(xD/2.2, yD/1.05, 12));
		allDrones.add(new BlockerDrone(xD/1.8, yD/1.1, 12));
		allDrones.add(new BlockerDrone(xD/1.8, yD/1.005, 12));
		allDrones.add(new BlockerDrone(xD/1.8, yD/1.05, 12));		
		allDrones.add(new BlockerDrone(xD/3, yD/5, 20));		
		allDrones.add(new BlockerDrone(xD/1.5, yD/5, 20));
		allDrones.add(new BlockerDrone(xD/2, yD/3, 15));
		allDrones.add(new BlockerDrone(xD/1.75, yD/6, 8));
		allDrones.add(new BlockerDrone(xD/2.35, yD/6, 8));
		allDrones.add(new BlockerDrone(xD/1.5, yD/3, 5));
		allDrones.add(new BlockerDrone(xD/3, yD/3, 5));
		allDrones.add(new BlockerDrone(xD/5, yD/1.5, 5));
		allDrones.add(new BlockerDrone(xD/1.25, 400, 5));	
		allDrones.add(new StaticEnemies(xD/1.25, 100, 5));
		allDrones.add(new StaticEnemies(xD/5, 100, 5));
		allDrones.add(new StaticEnemies(xD/1.25, 500, 5));
		allDrones.add(new StaticEnemies(xD/5, 500, 5));
	}

	protected void addDrone() { //This function will add the "bad" drones with their respective positions
		allDrones.add(new MBDrone(xArenaSize/5.5, yArenaSize/1.50, 5, 30, 1));
		allDrones.add(new MBDrone(xArenaSize/1.25, yArenaSize/1.50, 5, 30, 1));
		allDrones.add(new MBDrone(xArenaSize/2.86, yArenaSize/3, 5, 30, 0.5));
		allDrones.add(new MBDrone(xArenaSize/1.54, yArenaSize/3, 5, 30, 0.5));
		allDrones.add(new MODrone(xArenaSize/3, yArenaSize/6, 10, 30, 0.3));
		allDrones.add(new MODrone(xArenaSize/1.5, yArenaSize/6, 10, 30, 0.3));	
	}

	protected static double Randomx(double min, double max){ //This is to create random positions
		double x = (Math.random()*((max-min)+1))+min;
		return x;	
	}

	protected void addObstacle() {//This is to create obstacles at random positons
		allDrones.add(new RandomBlock(xArenaSize/Randomx(1,5), yArenaSize/Randomx(1,5), 10));
	}

	protected void addREnemies() {//This is to create obstacles at random positons
		allDrones.add(new RandomEnemies(xArenaSize/Randomx(1,5), yArenaSize/Randomx(1,5), 5));
	}
	
	
	
	protected void drawArena(MyDroneCanvas mc) {
		for (Drone d : allDrones) d.drawDrone(mc); //This will draw all the drones
				
	}
	
	
	protected void checkDrone() { //This will check all the drones
		for (Drone d : allDrones) d.checkDrone(this);	
	}
	
	
	protected void adjustDrone() { //Change positions of the drones
		for (Drone d : allDrones) d.adjustDrone();
	}

	
	protected ArrayList<String> describeAll() { //Array with all the types of drones in it
		ArrayList<String> ans = new ArrayList<String>(); 		
		for (Drone d : allDrones) ans.add(d.toString());			
		return ans;												
	}
	protected double CheckDroneAngle(double x, double y, double rd, double ang, int notIDDrone) { //Change the angle of the drone with hitting with a wall
		double ans = ang;
		if (x < rd || x > xArenaSize - rd) ans = 180 - ans;
			
		if (y < rd || y > yArenaSize - rd) ans = - ans;
			
		
		for (Drone d : allDrones) 
			if (d.getID() != notIDDrone && d.hittingDrone(x, y, rd)) ans = 180*Math.atan2(y-d.getY(), x-d.getX())/Math.PI;
				
		
		return ans;		
	}

	protected boolean DroneHit(Drone target) { //To check if the drone hits another drone
		boolean ans = false;
		for (Drone d : allDrones)
			if (d instanceof GameDrone && d.hittingDrone(target)) ans = true;
				
		return ans;
	}

	protected static double getX() {
		return xArenaSize; //get functions for X and Y returning the size of the arena
		
	}
	protected static double getY() {
		return yArenaSize;
		
	}
	protected void setCursor(double x, double y) { //setter function for the paddle
		for (Drone d : allDrones)
			if (d instanceof CursorDrone) d.setXY(x, y);
	
	}	
}

	

