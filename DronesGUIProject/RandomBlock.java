package DronesGUIProject;

public class RandomBlock extends Drone { //This class extends from the drone and relates to the creation of random obstacles
	

	public RandomBlock(double bx, double by, double br) {
		super(bx, by, br);
		colorr = 'd'; 
	}
	
	protected void checkDrone(DroneArena b) {		
	}
  
	
	protected void adjustDrone() {	
	}

	protected String getStrType() {
		return "Random Blocker";
	}	

}
