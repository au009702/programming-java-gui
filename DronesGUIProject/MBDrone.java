package DronesGUIProject;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class MBDrone extends Drone { //This class extends from the Drone and relates to the moving enemies

	double dAngle, dSpeed;			
	
	public MBDrone(double dx, double dy, double dr, double da, double ds) {
		super(dx, dy, dr);
		dAngle = da;
		dSpeed = ds;
        colorr = 'c';	
	}
	
    protected void checkDrone(DroneArena d) {
        if (d.DroneHit(this)) EndP(null);			
        dAngle = d.CheckDroneAngle(x, y, rd, dAngle, DroneTag);}


		private void EndP(Stage stage){
			Alert endAlert = new Alert(AlertType.INFORMATION, "Game Over");
			endAlert.setTitle("Game Over");
			endAlert.setHeaderText(null);
			endAlert.setOnCloseRequest(null);
			endAlert.show();
			System.exit(0);			
		}	
	protected void adjustDrone() {
        double radAngle = dAngle*Math.PI/180;		
		x += dSpeed * Math.cos(radAngle);		
		y += dSpeed * Math.sin(radAngle);
				
	}

	protected String getStrType() {
		return "Moving Enemy";
	}
}