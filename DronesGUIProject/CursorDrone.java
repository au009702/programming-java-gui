package DronesGUIProject;

public class CursorDrone extends Drone { //Class that hierarchies from Drone and relates to the cursor


	protected CursorDrone(double px, double py, double pr) {
		super(px, py, pr);
		colorr = 'b';		
	}

	protected void checkDrone(DroneArena da) {		
	}	
	
	protected void adjustDrone() { 
	}

	protected String getStrType() {
		return "CursorDrone";
	}	
}