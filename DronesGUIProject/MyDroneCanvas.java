package DronesGUIProject;
import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.text.TextAlignment;


public class MyDroneCanvas {
	int xDroneCanvasSize = 5000;			//These are constants for the size of the canvas	
	int yDroneCanvasSize = 5000;
    GraphicsContext cg; 
/**
     * constructor that createsGraphics context and size of canvas
     * @param g
     * @param x,y
     */
    
    protected MyDroneCanvas(GraphicsContext g, int xx, int yy) { //This will be the constructor
    	cg = g;
    	xDroneCanvasSize = xx;
    	yDroneCanvasSize = yy;
    }
    
   
    protected void clearDroneCanvas() { //This will clear the canvas everytime the drone moves
		cg.clearRect(0,  0,  xDroneCanvasSize,  yDroneCanvasSize);		
    }
    
	
	protected void drawDroneCanvas (Image o, double x, double y, double t) {
			 
		cg.drawImage(o, xDroneCanvasSize * (x - t/2), yDroneCanvasSize*(y - t/2), xDroneCanvasSize*t, yDroneCanvasSize*t);
	}
	
	
	Color droneColour (char clr) { //This is the function to convert the variable clr into a colour
		
		Color ans = Color.BLACK;
		switch (clr) {
		case 'a' :	ans = Color.CHOCOLATE;
					break;
		case 'b' :	ans = Color.CYAN;
		    		break;
		case 'c' :	ans = Color.MAROON;
					break;
		case 'd' :	ans = Color.MEDIUMSEAGREEN;
					break;
		case 'e' :	ans = Color.DARKTURQUOISE;
					break;
		case 'f' :	ans = Color.GOLDENROD;
					break;
		case 'g' :	ans = Color.SIENNA;
					break;
		case 'h' :	ans = Color.GOLD;
					break;
		case 'i' :	ans = Color.INDIGO;
					break;
		case 'j' :	ans = Color.BURLYWOOD;
					break;
	
		}
		return ans;	
	}
	
	protected void DronesetFillColour (Color c) {
		cg.setFill(c);
	}
	
	protected void showDrone(double x, double y, double rad, char colourr) {
	 	DronesetFillColour(droneColour(colourr));									
		cg.fillArc(x-rad, y-rad, rad*2, rad*2, 0, 360, ArcType.ROUND);	
	}

	
	protected void showDrone(double x, double y, double rad) {
		cg.fillArc(x-rad, y-rad, rad*2, rad*2, 0, 360, ArcType.ROUND);	
	}


	protected void showDroneText (double x, double y, String s) {
		cg.setTextAlign(TextAlignment.CENTER);							
		cg.setTextBaseline(VPos.CENTER);								
		cg.setFill(Color.WHITE);										
		cg.fillText(s, x, y);						
	}

  
	protected void showInt (double x, double y, int i) {
		showDroneText (x, y, Integer.toString(i));  
	}

	protected void setFill(Color blue) {
	}

	protected int getXDroneCanvasSize() { //Getter function that returns x from the size of the canvas
    	return xDroneCanvasSize;
    }
    
    protected int getYDroneCanvasSize() {
    	return yDroneCanvasSize;
    }

 
}

